% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 20.11.2012

\begin{notice*}[Frage:]
  Wann verschwindet die Fluktuation einer Größe $A$?
\end{notice*}

\[
  (\Delta A )^2 \stackrel{!}{=} 0 \iff \langle A \rangle^2= \langle A^2 \rangle
\]
oder
\[
  \int \psi^\ast \hat{A}^2 \psi \; \mathrm{d}x \stackrel{!}{=} \left(\int \psi^\ast \hat{A} \psi \mathrm{d}x \right)^2
\]
Eine hinreichende Bedingung hierfür ist:
\[
  \hat{A}\psi = \lambda \psi 
\]
Dies ist eine Eigenwertgleichung, wobei $\psi$ eine Eigenfunktion zum Operator $\hat{A}$ ist und $\lambda$ ein Eigenwert zur Wellenfunktion $\psi$.

Die Eigenwerte einer Matrix $\mathscr{M}$:
\[
  \mathscr{M} = \begin{pmatrix}
   M_{11} & \cdots & M_{1n} \\
   \vdots & \ddots & \vdots \\
   M_{n1} & \cdots & M_{nn}
  \end{pmatrix}
\]
werden mit dem charakteristischen Polynom berechnet:
\[
  \det(\mathscr{M} - \lambda \mathds{1}) = 0
\]
Ähnliche Matrix $\mathscr{M}'$:
%
\begin{align*}
  \mathscr{M}' &= \mathrm{diag}(\lambda_1, \ldots, \lambda_n) \\
  &= \begin{pmatrix} \lambda_1 & & 0 \\ & \ddots & \\ 0 & & \lambda_n \end{pmatrix}
\end{align*} 
%
Ist ein System im Eigenzustand des Operators $\hat{A}$, dann kann $A$ scharf gemessen werden $\iff \Delta A = 0$.

\begin{example*}
  Für Eigenzustände $\psi_n$ zu $\hat{H}$ gilt die Eigenwertgleichung:
  \[
    \boxed{\hat{H} \psi_n = E_n \psi_n}
  \]
  Dies ist die \acct[0]{zeitunabhängige Schrödingergleichung}\index{Schrödingergleichung!zeitunabhängig}. $E_n$ sind die Eigenwerte der Zustände $\psi_n$ (Orbitale).
\end{example*}

Somit erhält man
%
\begin{gather}
  \psi_n(t)= \psi(t=0) \exp\left(\frac{\mathrm{i}E_n t}{\hbar}\right)
\end{gather}
%
und für die Wahrscheinlichkeitsverteilung gilt:
%
\begin{align*}
  P(x,t) &= |\psi(x,t)|^2 \\
  &= \psi^\ast(x,0) \exp\left(-\frac{\mathrm{i}E_n t}{\hbar}\right) \psi(x,0) \exp\left(\frac{\mathrm{i}E_n t}{\hbar}\right) \\
  &= \psi^\ast(x,0) \psi(x,0) \\
  &= |\psi(x,0)|^2
\end{align*}
%
Dies gilt für $P(x,t)$ für alle Zeit $t$.

\begin{notice*}
  Alle Operatoren $\hat{A}$, die man mit $\hat{H}$ vertauschen, entsprechen Erhaltungsgrößen. Wenn also
  \begin{gather}
    [\hat{A},\hat{H}] = 0 ,
  \end{gather}
  dann ist $A$ eine Erhaltungsgröße.
\end{notice*}

\begin{example*}
\begin{item-triangle}
  \item Drehimpuls: $\bm{\ell} = \bm{r} \times \bm{p}$
  \item Drehimpulsoperator: $\hat{\bm{\ell}} = \mathrm{i} \hbar \bm{r} \times \nabla$
\end{item-triangle}
Im Zentralpotential $V=V(|\bm{r}|)=V(r)$ ist der Betrag des Drehimpulses eine Erhaltungsgröße:
\[
  [\hat{H},\hat{\ell}^2]= 0
\]
\end{example*}

Daraus folgt:
\begin{item-triangle}
  \item Energie und Drehimpuls können gleichzeitig und unabhängig voneinander scharf gemessen werden.
  \item Es gibt Zustände, die gleichzeitig Eigenzustände zu $\hat{H}$ und $\hat{\ell}$ sind.
  \item Ein stationärer Zustand kann durch weitere Größen, wie zum Beispiel die Eigenwerte des Drehimpulses oder durch die Projektion des Drehimpulses auf die $z$-Achse $\ell_z$ charakterisiert werden. (Quantenzahlen)
\end{item-triangle}

\begin{example*}
  Orbitale im Wasserstoffatom: $3s$, $3p$, $3d$, \ldots.
\end{example*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-0.4)(11,2.5)
    % s-Orbitale
    \psline(0,0)(1,0)
    \psline(0,1)(1,1)
    \psline(0,2)(1,2)
    % p-Orbitale
    \psline(1.5,1.2)(2.5,1.2) \psline(2.7,1.2)(3.7,1.2) \psline(3.9,1.2)(4.9,1.2)
    \psline(1.5,2.2)(2.5,2.2) \psline(2.7,2.2)(3.7,2.2) \psline(3.9,2.2)(4.9,2.2)
    % d-Orbitale
    \psline(5.4,2.4)(6.4,2.4) \psline(6.6,2.4)(7.6,2.4) \psline(7.8,2.4)(8.8,2.4) \psline(9,2.4)(10,2.4) \psline(10.2,2.4)(11.2,2.4)
    
    % Befüllen
    \rput(0,0){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(0,1){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(0,2){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(1.5,1.2){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(2.7,1.2){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(3.9,1.2){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(1.5,2.2){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(2.7,2.2){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(3.9,2.2){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(5.4,2.4){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(6.6,2.4){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(7.8,2.4){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(9,2.4){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    \rput(10.2,2.4){\psline[linecolor=DarkOrange3]{->}(0.4,0.1)(0.4,0.4) \psline[linecolor=DarkOrange3]{<-}(0.6,0.1)(0.6,0.4)}
    
    % Labels
    \uput[180](0,0){\color{DimGray} $1s$}
    \uput[180](0,1){\color{DimGray} $2s$}
    \uput[180](0,2){\color{DimGray} $3s$}
    \uput[-45](4.9,1.2){\color{DimGray} $2p$}
    \uput[-45](4.9,2.2){\color{DimGray} $3p$}
    \uput[0](11.2,2.4){\color{DimGray} $3d$}
    
    \uput[-90](2,1.2){\color{DimGray} \footnotesize $m=-1$}
    \uput[-90](3.2,1.2){\color{DimGray} \footnotesize $0$}
    \uput[-90](4.4,1.2){\color{DimGray} \footnotesize $1$}
    
    \uput[-90](5.9,2.4){\color{DimGray} \footnotesize $m=-2$}
    \uput[-90](7.1,2.4){\color{DimGray} \footnotesize $-1$}
    \uput[-90](8.3,2.4){\color{DimGray} \footnotesize $0$}
    \uput[-90](9.5,2.4){\color{DimGray} \footnotesize $1$}
    \uput[-90](10.7,2.4){\color{DimGray} \footnotesize $2$}
    
    \uput[-90](0.5,-0.2){\color{DimGray} $\ell = 0$, >>$s$<<}
    \uput[-90](3.2,-0.2){\color{DimGray} $\ell = 1$, >>$p$<<}
    \uput[-90](8.3,-0.2){\color{DimGray} $\ell = 2$, >>$d$<<}
  \end{pspicture}
\end{figure}
