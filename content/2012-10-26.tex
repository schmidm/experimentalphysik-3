% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 26.10.2012

\subsection{Polarisation des Lichtes}
Bei linearer Polarisation ist der $\bm{E}$-Vektor einer ebenen Welle konstant:
%
\begin{align*}
  \bm{E}_1 &= \bm{E}_{0,1} \exp(\mathrm{i}(kz-\omega t)) && \bm{E}_{0,1} = \begin{pmatrix} E_x \\ 0  \\ 0  \end{pmatrix} \\
  \bm{E}_2 &= \bm{E}_{0,2} \exp(\mathrm{i}(kz-\omega t)) && \bm{E}_{0,2} = \begin{pmatrix} 0 \\ E_y  \\ 0  \end{pmatrix} \\
\end{align*}
%
Beides sind Lösungen der Wellengleichung, ihre Superposition also auch:
%
\begin{align*}
  \bm{E}_1 \pm \bm{E_2} &= \bm{E}_{\text{ges}} \exp(\mathrm{i}(kz- \omega t))
\end{align*}
%
Nicht nur reelle, sondern auch komplexe Linearkombinationen von $\bm{E}_1$ und $\bm{E}_2$ sind Lösungen der Wellengleichung.
%
\begin{example*}
  Es soll folgender Fall betrachtet werden : $\bm{E}_1 + \mathrm{i} \bm{E_2}$. Hierfür gilt folgende Abbildung:
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-2,-2)(3.5,1.5)
      \pstThreeDCoor[linecolor=DimGray,linewidth=1pt,xMax=1.5,yMax=4,zMax=1.5,xMin=-0.3,yMin=-0.3,zMin=-0.3,nameX=\color{DimGray},nameY=\color{DimGray},nameZ=\color{DimGray}]
      \parametricplotThreeD[xPlotpoints=200,linecolor=MidnightBlue,plotstyle=curve,algebraic](0,14){sin(t+0.785398163397) | t/4 | cos(t+0.785398163397)}
      \pstThreeDLine[linecolor=DarkOrange3,arrows=->](0,0,0)(0.707106781187,0,0.707106781187)
      \pstThreeDLine[linecolor=DarkOrange3,arrows=->](0,0,0)(-0.707106781187,0,0.707106781187)
      \pstThreeDLine[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](0,0,0.707106781187)(0.707106781187,0,0.707106781187)
      \pstThreeDLine[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](0.707106781187,0,0)(0.707106781187,0,0.707106781187)
      \pstThreeDLine[linecolor=DarkRed,arrows=->](0,0,0)(0,0,0.707106781187)
      \pstThreeDLine[linecolor=DarkRed,arrows=->](0,0,0)(0.707106781187,0,0)
      \pstThreeDPut(0.4,0,-0.4){\color{DarkRed} $\bm{E}_2$}
      \pstThreeDPut(-0.4,0,0.4){\color{DarkRed} $\bm{E}_1$}
      \pstThreeDPut(1.2,0,1.2){\color{DarkOrange3} $\bm{E}_{\mathrm{ges}}^{+}$}
      \pstThreeDPut(-1.2,0,0.8){\color{DarkOrange3} $\bm{E}_{\mathrm{ges}}^{-}$}
    \end{pspicture}
  \end{figure}
  
  \begin{item-triangle}
    \item[$+$:] von vorne gesehen Linksschraube --- links zirkular polarisiertes Licht 
    \item[$-$:] von vorne gesehen Rechtssschraube --- rechts zirkular polarisiertes Licht 
  \end{item-triangle}
\end{example*}

Bei einer Phasenverschiebung von $\varphi \neq 90^\circ$, oder bei $\varphi = 90^\circ$ und $|\bm{E}_x|\neq|\bm{E}_y|$ spricht man von elliptischen polarisierten Licht. 

Allgemeine Überlagerung:
\[
  \bm{E}_{\text{ges}} = a_1 \bm{E}_1 + a_2 \bm{E}_2 \qquad, \qquad a_{1/2} \in \mathbb{C} 
\]
Dabei macht $|a_1|^2 + |a_2|^2$ nur eine Aussage über die Gesamtintensität. Eine gemeinsame absolute Phase von $a_1$ und $a_2$ beeinflussen den Polarisationszustand nicht. Die relative Phasendifferenz $\Delta \varphi$ zwischen $a_1$ und $a_2$ ist:
%
\begin{align*}
  a_1 &= |a_1| \exp(\mathrm{i} \varphi_1) \qquad \text{und} \qquad a_2 = |a_2| \exp(\mathrm{i} \varphi_2) 
  \intertext{Dies ist die Polardarstellung von $a_1$ und $a_2$.}
  \Rightarrow \Delta \varphi &= \varphi_2 - \varphi_1
\end{align*}
% 
Die relative Amplitude ist $a = {|a_1|}/{|a_2|}$

Darstellung des Polarisationszustandes auf der \acct{Stokes-Kugel}:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1.5)(1.5,1.7)
    \pscircle[linecolor=MidnightBlue](0,0){1.5}
    \psline[linecolor=DarkRed]{->}(0,0)(-0.707,-0.707)
    \psellipse[linecolor=MidnightBlue](0,0)(1.5,0.5)
    \psdots[linecolor=DarkOrange3](0,1.5)(0,-1.5)(1.5,0)(0,0.5)(0,-0.5)
    \uput[0](1.5,0){\color{DarkOrange3} $\frac{1}{\sqrt{2}} (E_x + E_y)$}
    \uput[30](0,1.5){\color{DarkOrange3} $E_y$}
    \uput[-30](0,-1.5){\color{DarkOrange3} $E_x$}
    \uput[-70](0,0.5){\color{DarkOrange3} $\frac{1}{\sqrt{2}} (E_x + \mathrm{i} E_y)$}
    \uput[-70](0,-0.5){\color{DarkOrange3} $\frac{1}{\sqrt{2}} (E_x + \mathrm{i} E_y)$}
    \rput{180}(-0.6,-0.6){
      \pscurve[linecolor=DarkRed](0.2,0)(0.8,0.1)(0.8,-0.1)(1.4,0)
      \uput[0]{180}(1.4,0){\color{DarkRed} Stokes-Vektor}
    }
  \end{pspicture}
  \caption*{Bei \url{http://demonstrations.wolfram.com/LightPolarizationAndStokesParameters/} kann man sich eine interaktive Animation zur Stokes-Kugel ansehen.}
\end{figure}

Die gegenüberliegenden Zustände sind orthogonal, bilden also eine Basis.

\minisec{Jones-Vektoren}
Die formale (und praktische) Beschreibung von Polarisation erfolgt durch normierte zweidimensionale Vektoren, die sogenannten \acct{Jones-Vektoren}. 
%
\begin{align*}
  \bm{E}_y &= \begin{pmatrix} 0 \\ 1 \end{pmatrix} && 
  \text{horizontal polarisiertes Licht} \\
  \bm{E}_x &= \begin{pmatrix} 1 \\ 0 \end{pmatrix} && 
  \text{vertikal polarisiertes Licht} \\
  \bm{E}_{\pm 45^\circ} &= \frac{1}{\sqrt{2}} \begin{pmatrix} \pm 1 \\ 1 \end{pmatrix} &&
  \text{linear polarisiertes Licht} \\
  \bm{E}_{\text{RCP}} &= \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ \mathrm{i} \end{pmatrix} && 
  \text{$\sigma_{-}$-polarisiertes Licht} \\
  \bm{E}_{\text{LCP}} &= \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ -\mathrm{i} \end{pmatrix} && 
  \text{$\sigma_{+}$-polarisiertes Licht} 
\end{align*}
%

\begin{example*}
Überlagerung von $\sigma_+$ und $\sigma_-$ - Licht:
%
\begin{align*}
  \bm{E}_{\text{ges}} &= \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ \mathrm{i} \end{pmatrix} + \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ -\mathrm{i} \end{pmatrix} 
  = \frac{2}{\sqrt{2}} \begin{pmatrix} 1 \\ 0\end{pmatrix} = \sqrt{2} \bm{E}_x 
\end{align*}
%
Es handelt sich also um linear polarisiertes Licht.
\end{example*}

\acct{Elliptisch polarisiertes Licht}:
%
\begin{align*}
  \bm{E}_{\text{ell}} &= \frac{1}{\sqrt{5}} \begin{pmatrix} 2 \\ -\mathrm{i} \end{pmatrix} 
\end{align*}
%
wenn:
%
\begin{align*}
  \bm{E}_1 \cdot \bm{E}_2 &= 0 \qquad \Leftrightarrow \qquad \text{$\bm{E}_1$ und $\bm{E}_2$ sind orthogonal}
\end{align*}
%
