% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 09.11.2012

und 
\[
  E = h \cdot \nu_{dB}
\]
$\psi$ ist im Gegensatz zu einer klassischen Wellenamplitude komplex. Es wird noch eine Interpretation benötigt, was $\pi$ physikalisch bedeutet.

Betrachte die Ableitung 
%
\begin{align*}
  \frac{\partial \psi}{\partial x} &= \frac{\mathrm{i} p}{\hbar} \psi \qquad \implies (-\mathrm{i} \hbar \partial_x ) \psi = p\psi \\
  \frac{\partial^2 \psi}{\partial x^2} &= \left(\frac{\mathrm{i} p }{\hbar}\right)^2 \psi = -\frac{p^2}{\hbar^2} \psi \\
  \frac{\partial \psi}{\partial t} &= -\frac{\mathrm{i} E }{\hbar} \psi \qquad \implies (\mathrm{i} \hbar \partial_t) \psi = E\psi \\
  \frac{\partial^2 \psi}{\partial t^2} &= -\frac{ E^2 }{\hbar^2} \psi
\end{align*}
%
\begin{enum-arab}
 \item Versuch der Konstruktion einer Wellengleichung analog zu klassischen Wellengleichung:
 %
 \begin{align*}
   \frac{\partial^2}{\partial x^2} \psi &\stackrel{?}{=} \frac{1}{v^2} \frac{\partial^2}{\partial t^2} \psi \\
   \implies \left[ -\frac{p^2}{\hbar^2} +\frac{E^2}{v^2 \hbar^2} \right] \psi &= 0 \\
   \implies E^2 &= p^2 v^2 
 \end{align*}
 %
 Dies steht im Widerspruch zu $E = {p^2}/{2m}$. Die Konstruktion analog zur klassischen Wellengleichung ist also erfolglos. 
 \item Versuch (Schrödinger):
 %
 \begin{align}
   \frac{p^2}{2m} \psi &= E \psi \nonumber \\
   p &\hateq \mathrm{i} \hbar \partial_x \nonumber 
 \end{align}
 \begin{align}
   \boxed{\left(-\frac{\hbar}{2m} \frac{\partial^2}{\partial x^2}\right) \psi = E \psi = \mathrm{i} \hbar \partial_t \psi}
 \end{align}
 %
 Dies ist die \acct[0]{Schrödingergleichung im feldfreien Raum}.\index{Schrödingergleichung!im feldfreien Raum}
\end{enum-arab}
%
Zu beachten ist hierbei, dass $\partial_t \psi$ vollständig durch $\psi$ bestimmt ist. Somit ist mit der Wellenfunktion $\psi$ der Zustand eines Systemes \acct[0]{vollständig} beschrieben, im Gegensatz zur klassischen Wellengleichung bei der zwei Anfangsbedingungen nötig sind.

\minisec{Differentialoperator}
%
\begin{align*}
  \underbrace{(\mathrm{i}\hbar \partial_x)}_{\hat{p}} \psi(x,t) &= p \psi(x,t) 
\end{align*}
%
Die Gleichung gilt nur bei ebenen Wellen (freie Teilchen) und stellt eine Eigenwertgleichung dar. Hierbei ist $\hat{p}$ der \acct{Impulsoperator} und $p$ ein Eigenwert. 

Die eindimensionale \acct[0]{Schrödingergleichung  mit ortsabhängigem Potential}\index{Schrödingergleichung!ortsabhängiges Potential} $V(x)$ lautet:
%
\begin{align}
\boxed{\mathrm{i}\hbar \partial_t \psi(x,t) = \underbrace{\left\{ -\frac{\hbar}{2m} \frac{\partial^2}{\partial x^2} + V(x)\right\}}_{\text{Hamiltonoperator} \, \hat{H}}  \psi (x,t)}
\end{align}
%
Die Kurzdarstellung sieht wie folgt aus:
%
\begin{align}
  \mathrm{i} \hbar \dot{\psi} &= H \psi
\end{align}
%
Die Gleichung wird auch also \acct[0]{zeitabhängige Schrödingergleichung}\index{Schrödingergleichung!zeitabhängig} bezeichnet.

Zum Lösen der Differentialgleichung wird der Ansatz der >>Separation der Variablen<< verwendet:
\[
  \psi(x,t) = u(x) f(t)
\]
Hierbei ist $u(x)$ der ortsabhängige- und $f(t)$ der zeitabhängige Anteil. Dieser Ansatz wird in die Schrödingergleichung eingesetzt und differenziert.
%
\begin{align*}
  \mathrm{i} \hbar \partial_t (u(x)f(t)) &= \hat{H} (u(x)f(t)) = f(t) \hat{H} u(x) \\
  \implies \underbrace{\frac{\mathrm{i} \hbar}{f(t)}}_{\text{zeitabhängig}} &= \underbrace{\frac{1}{u(x)} \hat{H} u(x)}_{\text{ortsabhängig}} \\
\end{align*}
%
Somit haben wir die Differentialgleichung in zwei Differentialgleichungen in $u(x)$ und $f(t)$ separiert. Für den linken Teil gilt:
\[
  \mathrm{i} \hbar \frac{1}{f} \frac{\partial f}{\partial x} = \mathrm{const}
\]
Mit Hilfe der Integration löst sich das Problem:
% 
\begin{align*}
  \mathrm{i} \hbar \int\limits_{0}^{t} \mathrm{d}t \; \frac{\partial f}{f \partial t} &= \int\limits_{0}^{t} \mathrm{const} \; \mathrm{d}t \\
  \implies \mathrm{i} \hbar \ln(f(t)) &= \mathrm{const} \cdot t
\end{align*}
%
Durch exponentieren\footnote{Neologismus von Prof. Dr. H. Giessen, eigentlich potenzieren} folgt:
\[
  f(t) = A \exp\left(\frac{\mathrm{const} \cdot t}{\mathrm{i} \hbar}\right) = A \exp(-\mathrm{i}\omega t)
\]
Mit $\omega = c/\hbar$  gilt für die Gleichung in $u(x)$:
%
\begin{align*}
  \frac{1}{u(x)} \left\{ -\frac{\hbar^2}{2m} \frac{\partial^2}{\partial x^2} + V(x) \right\} u(x) &= E \\
  \implies u''(x) + \frac{2m}{\hbar^2} \underbrace{\left\{ E - V(x) \right\}}_{= k^2(x)} u(x) &= 0
\end{align*}
%
Somit erhält man für die Ortskomponente analog zur Optik die sogenannte \acct{Helmholzgleichung}:
\[
  u''(x) + k^2(x) u(x) = 0
\]
Diese reelle Gleichung kann nur reelle Lösungen haben. Man setzt ein \emph{ortsunabhängiges} Potential an
\[
  V(x) = \mathrm{const} ,
\]
somit ist
\[
  k(x) = \mathrm{const} = \sqrt{\frac{2m}{\hbar^2} (E-V)} .
\]
Dann ist $u(x) = \alpha \exp(\mathrm{i}kx)$ eine spezielle Lösung für freie Teilchen.
%
\begin{align*}
  u'(x) &= \mathrm{i} k u(x) \\
  u''(x) &= -k^2 u(x)
\end{align*}
%
Für ein konstantes Potential ist 
\[
  \psi = u(x) f(t) = A \exp(\mathrm{i}(kx-\omega t))
\]
eine Lösung der Schrödingergleichung.
%
\begin{notice*}
  Ein freies quantenmechanisches Teilchen entspricht einer ebenen Welle!
\end{notice*}
%
Die Lösung lautet somit
\[
  \psi(x,t) = u(x) f(t) = A \exp(\mathrm{i}(kx-\omega t)) .
\]

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-2)(9.5,2.5)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.5,-0.5)(9.5,2)
    \psplot[linecolor=DarkOrange3,plotpoints=200]{0}{4}{0.8*cos(2*3.14159265359*x)+0.8}
    \rput(4,0){\psplot[linecolor=DarkOrange3,plotpoints=200]{0}{1}{1.6*2.71828182846^(-x)}}
    \psframe(4,0)(5,2)
    \rput(5,0){\psplot[linecolor=DarkOrange3,plotpoints=200]{0}{4}{0.294303552937*cos(2*3.14159265359*x)+0.294303552937}}
    \uput[90](2,2){\color{DimGray} $k^2 > 0$}
    \uput[90](4.5,2){\color{DimGray} $k^2 < 0$}
    \uput[90](7,2){\color{DimGray} $k^2 > 0$}
    \psbrace[fillcolor=MidnightBlue,bracePos=0.25](0,0)(4,0){}
    \uput[-90](2.2,-0.45){
      \parbox{4cm}{
        \color{MidnightBlue}\footnotesize
        imaginärer Exponent \\
        wg. $k^2 > 0$ $\to$ $k$ reell
        \[ \exp(\mathrm{i} k x) \]
        propagierende Lösung
      }
    }
    \psbrace[fillcolor=DarkRed](4,0)(5,0){}
    \uput[-90](4.5,-0.45){
      \parbox{3.4cm}{
        \color{DarkRed}\footnotesize
        reeller Exponent \\
        wg. $k^2 < 0$ $\to$ $k$ imaginär
        \[ \exp[\mathrm{i}(\mathrm{i} k x)] = \exp(-k x) \]
        exponentiell abfallende Lösung
      }
    }
    \psbrace[fillcolor=MidnightBlue,bracePos=0.75](5,0)(9,0){}
    \uput[-90](9,-0.45){
      \parbox{4cm}{
        \color{MidnightBlue}\footnotesize
        imaginärer Exponent \\
        wg. $k^2 > 0$ $\to$ $k$ reell
        \[ \exp(\mathrm{i} k x) \]
        propagierende Lösung
      }
    }
  \end{pspicture}
\end{figure}

Das Wellenpaket ist Lösung der Wellengleichung. Wie bei den elektromagnetischen Wellen gilt eine Fourierrelation:
\[
   \Delta x \cdot \Delta p \geq 1
\]
Mit 
\[
  p= \hbar k
\]
folgt
\[
  \Delta p = \hbar \Delta k
\]
Dies führt auf die \acct{Heisenbergsche Unschärferelation}:
%
\begin{gather}
  \boxed{\Delta x \cdot \Delta p \geq \hbar}
\end{gather}
%
