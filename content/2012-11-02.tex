% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 02.11.2012

\section{Materialwellen}

\minisec{Phasen- und Gruppengeschwindigkeit}
Ebene Wellen haben die Form:
\[
  E_x = E_0 \exp(\mathrm{i}(kz-\omega t + \phi))
\]
Daraus erhält man die sogenannte Dispersionsrelation:
\[
  \omega(k) = c \cdot k
\]
durch Einsetzen in die Wellengleichung.

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(2,2)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.5,-0.5)(2,2)[\color{DimGray} $k = \dfrac{2 \pi}{\lambda} = \dfrac{2 \pi f}{c} = \dfrac{\omega}{c}$,0][\color{DimGray} $\omega$,180]
    \psline(0,0)(2,2)
    \psline[linecolor=DarkOrange3](1,1)(1.5,1)(1.5,1.5)
    \uput[-90](1.25,1){\color{DarkOrange3} $1$}
    \uput[0](1.5,1.25){\color{DarkOrange3} $c$}
  \end{pspicture}
\end{figure}

Nach dem Superpositionsprinzip ergeben sich neue Lösungen der Wellengleichung als Summe von Lösungen. Ein \acct{Wellenpaket} setzt sich als Linearkombination vieler ebener Wellen im Intervall $[k_0 - \Delta k, k_0 + \Delta k ]$ zusammen. Somit erhalten wir für: 
%
\begin{align*}
  E_x(z,t) &= E_0 \int\limits_{k_0 -\Delta k}^{k_0 +\Delta k} \exp(\mathrm{i}(kz- \omega(k) t)) \; \mathrm{d}k
\end{align*}
%
Entwickelt man nun $\omega(k)$ für $k \ll k_0$ um $k_0$ herum als Taylorreihe, so erhält man:
%
\begin{align*}
  \omega(k) &=  \omega(k_0) + (k-k_0) \frac{\partial \omega}{\partial k}\Bigg|_{k=k_0} + (k-k_0)^2 \frac{1}{2} \frac{\partial^2 \omega}{\partial k^2} \Bigg|_{k=k_0} + \mathcal{O}(k^3) 
\end{align*}
%
Hierbei ist $\left( {\partial \omega }/{\partial k} \right)_{k_0}$ die \acct{Gruppengeschwindigkeit}, mit der sich die Energie und somit die Information ausbreitet. $\left( {\partial^2 \omega}/{\partial k^2} \right)_{k_0}$ entspricht der \acct{Gruppengeschwindigkeitsdispersion}. In reeller Schreibweise erhält man:
%
\begin{align*}
  E_x(z,t) &= E_0 \int\limits_{k_0 -\Delta k}^{k_0 +\Delta k} \cos((kz-\omega(k))t) \; \mathrm{d}k
\end{align*}
%
Bricht man die Taylorentwicklung nach dem zweiten Glied ab, erhält man:
%
\begin{align*}
  \omega(k) &= \omega(k_0) + (k-k_0) c_g(k_0)
\intertext{$c_g$ entspricht der Gruppengeschwindigkeit. Somit ist:}
  E_x &= E_0 \int \cos(kz- \underbrace{\omega(k_0)}_{\omega_0} t - (k-k_0) c_g(k_0) t) \; \mathrm{d}k \\
  &= 2 E_0 \frac{\Delta k}{2 \pi} \underbrace{\frac{\sin(\Delta k (c_g t - z))}{\underbrace{\Delta k (c_g t -z)}_{= \xi}}}_{\mathrm{sinc}\text{-Funktion}} \underbrace{\cos(\omega_0 t -k_0 z)}_{\cos\text{-Trägerwelle}}  
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-2.2)(11,2.5)
    \rput(0,0){
      \psaxes[ticks=none,labels=none,yAxis=false]{->}(0,0)(0,0)(3,0)[\color{DimGray} $k$,0][,0]
      \psline(1,-0.1)(1,1)(2,1)(2,-0.1)
      \psline(1.5,0.1)(1.5,-0.1)
      \uput[-90](0.5,0){\color{DimGray} $k_0 - \Delta k$}
      \uput[-90](1.5,0){\color{DimGray} $k_0$}
      \uput[-90](2.5,0){\color{DimGray} $k_0 + \Delta k$}
    }
    \pnode(3,1){A}
    \pnode(6,1){B}
    \ncarc[arrows=->,arcangle=30]{A}{B}
    \naput{\color{DimGray} Fourier-Transformation}
    \rput(8,0){
      \psaxes[ticks=none,labels=none]{->}(0,0)(-3,-2.2)(3,2.5)[\color{DimGray} $x$,0][\color{DimGray} $y$,180]
      \psplot[plotpoints=400,linecolor=MidnightBlue]{-2.5}{2.5}{0.5*sin(4*x)/x}
      \psplot[plotpoints=400,linecolor=DarkOrange3]{-2.5}{2.5}{0.5*sin(4*x)/x*cos(15*x)}
    }
  \end{pspicture}
\end{figure}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-2.2)(11,2.5)
    \rput(0,0){
      \psaxes[ticks=none,labels=none,yAxis=false]{->}(0,0)(0,0)(3,0)[\color{DimGray} $k$,0][,0]
      \psline(0.5,-0.1)(0.5,1)(2.5,1)(2.5,-0.1)
      \psline(1.5,0.1)(1.5,-0.1)
      \uput[-90](0.5,0){\color{DimGray} $k_0 - \Delta k$}
      \uput[-90](1.5,0){\color{DimGray} $k_0$}
      \uput[-90](2.5,0){\color{DimGray} $k_0 + \Delta k$}
    }
    \pnode(3,1){A}
    \pnode(6,1){B}
    \ncarc[arrows=->,arcangle=30]{A}{B}
    \naput{\color{DimGray} Fourier-Transformation}
    \rput(8,0){
      \psaxes[ticks=none,labels=none]{->}(0,0)(-3,-2.2)(3,2.5)[\color{DimGray} $x$,0][\color{DimGray} $y$,180]
      \psplot[plotpoints=400,linecolor=MidnightBlue]{-2.5}{2.5}{0.25*sin(8*x)/x}
      \psplot[plotpoints=400,linecolor=DarkOrange3]{-2.5}{2.5}{0.25*sin(8*x)/x*cos(15*x)}
    }
  \end{pspicture}
\end{figure}

Man sieht, dass die Einhüllende des Wellenpakets mit der Gruppengeschwindigkeit
\[
  c_g = \frac{\partial \omega}{\partial k}\Bigg|_{k=k_0}
\]
propagiert.

Die einzelnen Wellenfronten ({\color{DarkOrange3}orange eingezeichnet}) propagieren mit der \acct{Phasengeschwindigkeit}:
\[
  c_P = \frac{\omega_0}{k_0} .
\]
Wir haben die Dispersion höherer Ordnung vernachlässigt. Falls ${\partial^2 \omega}/{\partial k^2} = 0$ ist, gibt es keine Dispersion und das Wellenpaket zerfließt nicht. 

Im Allgemeinen liegt in einem Medium oder in einem Wellenleiter eine nicht-triviale Dispersion $\omega(k)$ vor.

\begin{example*}
Plasmonendispersion:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-0.5)(3,2.5)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.5,-0.5)(3,2.5)[\color{DimGray} $k$,0][\color{DimGray} $\omega(k)$,180]
    \psplot[plotpoints=400,linecolor=MidnightBlue]{0}{2.3}{1.5*(1-2.718281828459045^(-3*x))}
    \psplot{0}{0.5}{4.5*x}
    \psline[linestyle=dashed,linecolor=DarkOrange3](-0.1,1.5)(2.3,1.5)
    \uput[0](0.8,2){\color{DimGray} Lichtgerade}
    \uput[180](-0.1,1){\color{DarkOrange3} Plasmafrequenz $\omega_P$}
    \uput[-90](2,1){\color{MidnightBlue} Plasmadispersion}
  \end{pspicture}
\end{figure}

\vspace*{-4em} ~ % Dirty endsymbol fix
\end{example*} 
