% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 30.10.2012

\minisec{Elliptische Polarisation}
Für eine Ellipse gilt die Gleichung:
%
\begin{align*}
  \frac{x^2}{a^2} + \frac{y^2}{b^2} &= 1
\end{align*}
%
Hierbei bezeichnet $a$ die große und $b$ die kleine Halbachse.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-1.5)(2,1.5)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-2,-1.5)(2,1.5)[\color{DimGray} $x$,-90][\color{DimGray} $y$,180]
    \psellipse(0,0)(1.5,1)
    \psline[linecolor=DarkOrange3](0,0)(1.5,0)
    \uput[-90](0.75,0){\color{DarkOrange3} $a$}
    \psline[linecolor=MidnightBlue](0,0)(0,1)
        \uput[180](0,0.5){\color{MidnightBlue} $b$}
  \end{pspicture}
  \vspace*{-2em}
\end{figure}

\begin{align*}
  \begin{pmatrix} x \\ y \end{pmatrix} &= \begin{pmatrix} a \cos t \\ b \sin t \end{pmatrix} \; , \quad t \in [0,1) \\
  \varepsilon &= \frac{\sqrt{a^2 - b^2}}{a} \; , \quad \varepsilon \in [0,1]
\end{align*}
%
\begin{item-triangle}
 \item $\varepsilon=0$: zirkular polarisiertes Licht
 \item $\varepsilon=1$: linear polarisiertes Licht
\end{item-triangle}
%
Lage der großen Halbachse kann vom Winkel $\varphi$ gedreht werden.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-1.5)(2,1.5)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-2,-1.5)(2,1.5)[\color{DimGray} $x$,-90][\color{DimGray} $y$,180]
    \psarc{->}(0,0){1.8}{0}{20}
    \uput{2}[10](0,0){\color{DimGray} $\varphi$}
    \rput{20}(0,0){
      \psaxes[ticks=none,labels=none](0,0)(-2,-1.5)(2,1.5)
      \psellipse(0,0)(1.5,1)
      \psline[linecolor=DarkOrange3](0,0)(1.5,0)
      \uput[-90](0.75,0){\color{DarkOrange3} $a$}
      \psline[linecolor=MidnightBlue](0,0)(0,1)
      \uput[180](0,0.5){\color{MidnightBlue} $b$}
    }
  \end{pspicture}
  \vspace*{-2em}
\end{figure}

\minisec{Allgemeine Ellipsengleichung}

Die \acct{allgemeine Ellipsengleichung} lautet:
%
\begin{align*}
  \begin{pmatrix} x \\ y \end{pmatrix} &= \begin{pmatrix} x_0 +a \cos t \cos \varphi - b \sin t \sin \varphi \\ y_0 + b \cos t \sin \varphi + b \sin t \cos \varphi \end{pmatrix} \; , \quad t \in [0,2\pi) 
\end{align*}
%
In Komponentenschreibweise der $\bm{E}$-Felder in $x$- und $y$-Richtung gilt:
%
\begin{gather*}
  \begin{aligned}
    E_x(z,t) &= E_{0x} \cos(kz-\omega t) \\
    E_y(z,t) &= E_{0y} \cos(kz-\omega t + \varepsilon)
  \end{aligned} \\
  \Rightarrow \frac{E_x^2}{E_{0x}^2} + \frac{E_y^2}{E_{0y}^2} - 2 \frac{E_x}{E_{0x}} \frac{E_y}{E_{0y}^2} \cos \varepsilon = \sin^2 \varepsilon
\end{gather*}
% 
\minisec{Messung der Polarisation}
Für einen idealen Polarisator in $y$-Richtung gilt:
%
\begin{align*}
  \bm{E}_{\text{in}} &= \begin{pmatrix} E_x \\ E_y \end{pmatrix} \qquad \bm{E}_{\text{out}}= \begin{pmatrix} 0 \\ E_y \end{pmatrix} 
\end{align*}
%
Die $x$-Komponente wird durch den Polarisator absorbiert (Polymer-Polarisator) oder reflektiert (Calcit). 

Mit Matrizen kann man Polarisationsfilter beschreiben:
%
\begin{align*}
  \mathscr{P}_x &= \begin{pmatrix} 1  &  0 \\ 0 & 0 \end{pmatrix} \qquad
  \mathscr{P}_y = \begin{pmatrix} 0  &  0 \\ 0 & 1 \end{pmatrix}
\end{align*}
%
Für allgemeine Filter $\mathscr{P}$ berechnet sich die ausgehende Polarisation zu:
\[
  \bm{E}_{\text{out}} = \mathscr{P} \cdot \bm{E}_{\text{in}}
\]
Um einen gedrehten Polarisator zu berechnen, drehen wir zuerst in ein neues Koordinatensystem, führen die Messung aus und drehen dann ins ursprüngliche Koordinatensystem zurück.

Die Drehmatrix ins gedrehte System ist:
%
\begin{align*}
  \mathscr{R}_\alpha &= \begin{pmatrix} \cos \alpha  &  -\sin \alpha \\ \sin \alpha & \cos \alpha \end{pmatrix} \stackrel{\alpha = 45^\circ}{=} \frac{1}{\sqrt{2}} \begin{pmatrix} 1  &  -1 \\ 1 & 1 \end{pmatrix}
\end{align*}
%
Dann wird die Messung durchgeführt von:
\[
  \mathscr{P}_y = \begin{pmatrix} 0  &  0 \\ 0 & 1 \end{pmatrix}
\]
Die Rücktransformation ist:
\[
  \mathscr{R}_{-45^\circ} = \frac{1}{\sqrt{2}} \begin{pmatrix} 1  &  1 \\ -1 & 1 \end{pmatrix}
\]
Somit ergibt sich:
\[
  \mathscr{P}_{45^\circ} = \frac{1}{\sqrt{2}} \begin{pmatrix} 1  &  1 \\ -1 & 1 \end{pmatrix} \begin{pmatrix} 0  &  0 \\ 0 & 1 \end{pmatrix} \frac{1}{\sqrt{2}} \begin{pmatrix} 1  &  -1 \\ 1 & 1 \end{pmatrix} = \frac{1}{2} \begin{pmatrix} 1  &  1 \\ 1 & 1 \end{pmatrix}
\]
Man kann $n$ polarisierende Elemente hintereinander schalten. Zur Berechnung der Gesamtpolarisation ist nur eine Matrixmultiplikation nötig:
\[
  \mathscr{P}_{\text{ges}} = \mathscr{P}_{n} \cdot \mathscr{P}_{n-1} \cdot \ldots \cdot \mathscr{P}_{2} \cdot \mathscr{P}_{1}
\]
Da ein Polarisator immer nur \emph{eine} Komponente misst, braucht man 2 Messungen um den Polarisationszustand zu berechnen.

\begin{example*}
Exzentrizität $\varepsilon$ und die Lage der großen Halbachse $\varphi$.

\minisec{1. Messung}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-0.5)(7,2)
    \psline{->}(0,0)(3,0)
    \uput[90](1.5,0){\color{DimGray} $\bm{E}_{\text{in}}$}
    \uput[-90](1.5,0){\color{DimGray} unbek. Pol.richtung}
    
    \psframe(3,-0.5)(4,0.5)
    \psline(3,0)(5,0)
    \psline(3.5,-0.5)(3.5,1.5)
    \psline(3,-0.5)(4,0.5)
    \psline{<->}(4.5,-0.3)(4.5,0.3)
    \uput[-90](4.5,-0.3){\color{DimGray} $\bm{E}_x$}
    \psdot(3.5,1)
    \uput[0](3.5,1){\color{DimGray} $\bm{E}_y$}
    
    \psframe[linecolor=DarkOrange3](5,-0.2)(5.4,0.2)
    \uput[0](5.4,0){\color{DarkOrange3} $I_1 \sim |E_x|^2$}
    \psframe[linecolor=DarkOrange3](3.3,1.5)(3.7,1.9)
    \uput[0](3.7,1.7){\color{DarkOrange3} $I_2 \sim |E_y|^2$}
  \end{pspicture}
\end{figure}

Die Messung liefert die beiden Komponenten $|\bm{E}_x|$, $|\bm{E}_y|$ oder nicht deren relative Phase.

\minisec{2. Messung}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-2)(7,0.5)
    \psline{->}(0,0)(3,0)
    \uput[90](1,0){\color{DimGray} $\bm{E}_{\text{in}}$}
    
    \psframe(1.5,-0.5)(1.8,0.5)
    \psline(1.5,-0.5)(1.8,0.5)
    \uput[-90](1.65,-0.5){\color{DimGray} $\lambda/4$ Platte}
    
    \psframe(3,-0.5)(4,0.5)
    \psline(3,0)(5,0)
    \psline(3.5,0.5)(3.5,-1.5)
    \psline(4,-0.5)(3,0.5)
    \psline{<->}(4.5,-0.3)(4.5,0.3)
    \uput[-90](4.5,-0.3){\color{DimGray} $\bm{E}_{\sigma_{-}}$}
    \psdot(3.5,-1)
    \uput[0](3.5,-1){\color{DimGray} $\bm{E}_{\sigma_{+}}$}
    
    \psframe[linecolor=DarkOrange3](5,-0.2)(5.4,0.2)
    \uput[0](5.4,0){\color{DarkOrange3} $I_1 \sim |E_{\sigma_{-}}|^2$}
    \psframe[linecolor=DarkOrange3](3.3,-1.5)(3.7,-1.9)
    \uput[0](3.7,-1.7){\color{DarkOrange3} $I_2 \sim |E_{\sigma_{+}}|^2$}
  \end{pspicture}
\end{figure}

Das $\lambda/4$-Plättchen führt zu einer polarisationsabhängigen Phasenverschiebung $\begin{pmatrix} 1  &  0 \\ 0 & \mathrm{i} \end{pmatrix}$.
%
\begin{align*}
  \mathscr{P}_{\lambda/4} &= \mathscr{R}_{45^\circ} \cdot \begin{pmatrix} 1  &  0 \\ 0 & \mathrm{i} \end{pmatrix} \cdot \mathscr{R}_{-45^\circ} = \frac{1}{2} \begin{pmatrix} 1  &  \mathrm{i} \\ \mathrm{i} & 1 \end{pmatrix}
\end{align*}
%
Durch den polarisierenden Strahlteiler wird $E_{\sigma_\pm}$ auf $\pm E_{x,y}$ abgebildet. Diese Messung liefert also $|E_{\sigma_+}|$ und $|E_{\sigma_-}|$. 
\end{example*}

Folglich erlaubt diese >>Polarisationstomographie<< die Rekonstruktion des Polarisationszustandes. Wichtig ist, dass eine \acct[0]{einzelne} Messung den Polarisationszustand nicht rekonstruieren kann. 

\begin{notice*}[Trick:]
  Benutze Einzelphotonenquelle, dann kann der Polarisationszustand einzelner Photonen nicht gemessen und kopiert werden (>>sichere<< Quantenkommunikation). 
\end{notice*}
