% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 30.11.2012

\subsection*{Freies Elektronengas-Modell der Metalle (Drude Modell)}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(3,1)
    \psline(0,1)(3,1)
    \psline(0,0)(3,0)
    \psRandom[dotsize=1pt,fillstyle=solid,linecolor=DarkOrange3,randomPoints=500](0,0)(3,1){
      \psframe[linestyle=none](0,0)(3,1)
    }
    \uput[-90](0,0){\color{DimGray} $-$}
    \uput[-90](3,0){\color{DimGray} $+$}
    \psline{->}(1,-0.1)(2,-0.1)
    \uput[-90](1.5,0){\color{DimGray} $e^{-}$}
    
    \uput[0](3,0.5){\color{DimGray} Kupfer-Kabel}
  \end{pspicture}
\end{figure}

Da die Elektronen in Metallen frei sind, gibt es keine Rückstellkraft (Federkonstante $=0$). Es gilt die Differenzialgleichung des harmonischen Oszillator:
%
\begin{align*}
  m \ddot{x} + \gamma \dot{x} + \underbrace{D}_{=0} x &= F \cos(\omega t) 
\end{align*}
%
Mit 
%
\begin{align*}
  \omega_0 &= \sqrt{\frac{D}{m}} = 0 
\end{align*}
%
Das Drude Modell ist analog zum Lotertzmodell mit $\omega_0 = 0$. Somit folgt für $\varepsilon'$ und $\varepsilon''$:
%
\begin{align*}
  \varepsilon'(\omega) &= 1- \frac{n_e e^2 }{\varepsilon_0 m} \frac{1}{\omega^2+\Gamma^2} \\
  \varepsilon''(\omega) &= - \frac{n_e e^2}{\varepsilon_0 m} \frac{\Gamma}{\omega^2 \left(\omega^2 + \Gamma^2\right)}
\end{align*}
%
\begin{notice}[Frage:]
Was ist der Ursprung der Dämpfung $\Gamma$? 
\end{notice}
%
Ursprung der Dämpfung sind Stoßprozess, die mit einer mittleren Stoßzeit $\tau_e$ erfolgen (Drude Modell).

Wird angenommen, dass $F=0$ so lautet die Differentialgleichung und deren Lösung für die Geschwindigkeit $v$:
%
\begin{align*}
  \frac{\mathrm{d}^2}{\mathrm{d}t^2}x &= 0 \\
  \implies \bm{v} = \dot{\bm{x}} &= - \frac{e}{m \Gamma} \bm{E}
\end{align*}
%
Wird angenommen, das nach jedem Stoß eine zufällig verteilte Geschwindigkeit vorliegt, so ist die mittlere Geschwindigkeit das Produkt aus Beschleunigung und mittlerer Stoßzeit ($v=a t$).
\[
  \dot{\bm{x}} = -\frac{e}{m} \bm{E} \tau_e
\]
Der Zusammenhang zwischen Dämpfung $\Gamma$ und mittlerer Stoßzeit $\tau_e$ ist
\[
  \Gamma = \frac{1}{\tau_e}.
\]
Für die Stromdichte gilt
\[
  \bm{j} = - n_e e \dot{\bm{x}} = \frac{n_e e^2}{\Gamma m} \bm{E} 
\]
Hierbei ist $n_e$ die Ladungsdichte.

Das \acct{ohmsche Gesetz} lautet
\[
  \bm{j} =  \sigma_e \bm{E}.
\]
Hierbei ist $\sigma_e$ die Leitfähigkeit eines Stoffes. Für die Dämpfung folgt somit
\[
  \Gamma = \frac{1}{\tau_e} = \frac{n_e e^2}{\sigma_e m}
\]
%
\begin{example*}
Für Aluminium ist $\sigma_e = 36 \cdot 10^{6} \, \mathrm{\frac{1}{\Omega m}}$ und $n_e = 0.18 \cdot 10^{30} \,  \frac{1}{\mathrm{m}^3}$.
%
\begin{align*}
    \tau_e &= 7 \cdot 10^{-15} \, \mathrm{s} \\
    &= 7 \, \mathrm{fs}
\end{align*}
%
\end{example*}
%
Rotes Licht hat im Vergleich dazu  eine Wellenlänge von $800$ nm und eine Periodendauer von $2.6$ fs.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-0.5)(3,0.5)
    \psaxes[labels=none,ticks=none,yAxis=false]{->}(0,0)(-0.2,-0.5)(3,0.5)[\color{DimGray} $t$,0][\color{DimGray},0]
    \psplot{-0.2}{2.8}{0.5*sin(\psPi*x)}
    \psline{|-|}(0.5,0.6)(2.5,0.6)
    \rput*(1.5,0.6){\color{DimGray} $2.6$ fs}
  \end{pspicture}
\end{figure}

Man führt die \acct{Plasmafrequenz} ein, welche wie folgt definiert ist:
\[
  \omega_P^2 =  \frac{n_e e^2}{\varepsilon_0 m}
\]
Hierbei entspricht $n_e$ der Dichte der Elektronen.

Umschreiben der Real- und Imaginärteile der dielektrischen Permittivität:
%
\begin{align*}
  \varepsilon'(\omega) &= 1- \frac{\omega_P^2 \tau_e^2}{1+ \omega^2 \tau_e^2} \\
  \varepsilon''(\omega) &= - \frac{\omega_P^2 \tau_e^2}{\omega \left(1+ 1+ \omega^2 \tau_e^2\right)}
\end{align*}
%
\begin{example*}
Für Aluminium bedeutet dies:
%
\begin{align*}
  \omega_P &= 24 \cdot 10^{15} \, \mathrm{Hz} \\
  \lambda_P &= 78 \, \mathrm{nm} \\
  E_P &= 15.8 \, \mathrm{eV}
\end{align*}
%
\end{example*}
%
Für Frequenzen unterhalb der Plasmafrequenz bedeutet dies, dass ein großer Imaginärteil von $\varepsilon(\omega)$, starke Absorption und große Reflexion vorliegt (Herleitung durch Umrechnung von $\varepsilon'$ und $\varepsilon''$ in $n$ und $\kappa$). Es folgt
\[
  R = \left|\frac{\tilde{n}-1}{\tilde{n}+1} \right|^2 \quad \text{mit} \quad \tilde{n} = 1+ \kappa .
\]

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-0.2)(3,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-0.2,-0.2)(3,1.3)[\color{DimGray} $\omega$,0][\color{DimGray} $R$,0]
    \psline[linearc=0.2,linecolor=MidnightBlue](0,1)(1.5,1)(1.5,0.5)
    \psline[linearc=0.2,linecolor=MidnightBlue](1.5,0.5)(1.5,0)(2.8,0)
    \psline(1.5,0.1)(1.5,-0.1)
    \psline(0.1,1)(-0.1,1)
    \uput[-90](1.5,0){\color{DimGray} $\omega_p$}
    \uput[180](0,1){\color{DimGray} $1$}
  \end{pspicture}
  
  \begin{pspicture}(-0.2,-0.2)(3,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-0.2,-0.2)(3,1.3)[\color{DimGray} $\lambda$,0][\color{DimGray} $R$,0]
    \psline[linearc=0.2,linecolor=MidnightBlue](0,0)(1.5,0)(1.5,0.5)
    \psline[linearc=0.2,linecolor=MidnightBlue](1.5,0.5)(1.5,1)(2.8,1)
    \psline(1.5,0.1)(1.5,-0.1)
    \psline(0.1,1)(-0.1,1)
    \uput[-90](1.5,0){\color{DimGray} $\lambda_p$}
    \uput[180](0,1){\color{DimGray} $1$}
  \end{pspicture}
\end{figure}

Für ein Frequenz, die oberhalb der Plasmafrequenz liegt $\omega > \omega_P$ ist $\omega \tau_e >> 1$ und es gilt:
%
\begin{align*}
  \varepsilon'(\omega) &\approx 1- \frac{\omega_P^2}{\omega^2} \quad , \quad \varepsilon''(\omega) \approx 0 \\
  \implies n(\omega) &= \sqrt{\varepsilon} \approx \sqrt{1- \frac{\omega_P^2}{\omega^2}} = 1- \frac{\omega_P^2}{2 \omega^2}
\end{align*}
%
Das heißt,  dass bei sehr kurzen Wellenlängen unterhalb der Plasmafrequenz (tiefes UV, weiche Röntgenstrahlung) sich Metalle wie Dielektriaka verhalten mit Brechungsindex 
\[
  n(\omega) \approx 1- \frac{\omega_P^2}{2 \omega^2}.
\]  

Metamaterial (künstliches Plasma mit Ladungsdichte $n_{\textbf{eff}}$)
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-0.5)(4,1.5)
    \psline(0,1)(4,1)
    \psline(0,0)(4,0)
    \psline(1,1.5)(1,-0.5)
    \psline(2,1.5)(2,-0.5)
    \psline(3,1.5)(3,-0.5)
    \rput(1,1){\psline(0.7;200)(0.7;20)}
    \rput(2,1){\psline(0.7;200)(0.7;20)}
    \rput(3,1){\psline(0.7;200)(0.7;20)}
    \rput(1,0){\psline(0.7;200)(0.7;20)}
    \rput(2,0){\psline(0.7;200)(0.7;20)}
    \rput(3,0){\psline(0.7;200)(0.7;20)}
    \uput[0](4,1){\color{DimGray} $n_e$}
    \pscircle[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](1.5,0.5){0.9}
    \uput{1.1}[140](1.5,0.5){\color{DarkOrange3} $n_{\text{eff}}$}
  \end{pspicture}
  \hspace*{4em}
  \begin{pspicture}(0,-0.5)(1,1.5)
    \psline(0,1)(0,0)(1,0)
    \psdots[dotstyle=o](0,1)(0,0)(1,0)
    \uput[0](0,1){\color{DimGray} $R$}
    \uput[180](0,0.5){\color{DimGray} $a$}
    \uput[-90](0.5,0){\color{DimGray} $a$}
  \end{pspicture}
\end{figure}

\[
  n_{\text{eff}} = \frac{R^2}{a^2} n_e
\]
Solange die Wellenlänge der elektromagnetischen Strahlung ca $10 \times$ größer ist als die Einheitszelle (Drahtabstand), sieht die Welle nur ein >>effektives Medium<< mit Ladungsdichte $n_{\text{eff}}$.
